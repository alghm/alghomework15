﻿// Создание узлов
Node node0 = new Node { Value = 0, Neighbors = new List<Node>() };
Node node1 = new Node { Value = 1, Neighbors = new List<Node>() };
Node node2 = new Node { Value = 2, Neighbors = new List<Node>() };
Node node3 = new Node { Value = 3, Neighbors = new List<Node>() };
Node node4 = new Node { Value = 4, Neighbors = new List<Node>() };

// Добавление ребер между узлами
// в данном случае ожидаем две компоненты сильной связности - 0,1,2 и 3,4
node0.Neighbors.Add(node1);
node1.Neighbors.Add(node2);
node2.Neighbors.Add(node0);
node3.Neighbors.Add(node4);
node4.Neighbors.Add(node3);

// Создание графа
Graph graph = new Graph { Nodes = new List<Node> { node0, node1, node2, node3, node4 } };

Console.WriteLine("Вывод графа");
PrintGraph(graph);

// Вызов алгоритма Косарайю
var result = KosarajuAlg(graph);

Console.WriteLine("Компоненты сильной связности:");
var components = result.GroupBy(x => x).Select(g => new { value = g.Key }).OrderBy(x => x.value);
foreach (var component in components)
{
    var val = component.value;
    var nodes = result
        .Select((value, index) => new { value, index })
        .Where(x => x.value == component.value)
        .Select(x => x.index);

    Console.WriteLine($"Компонента {val} - {String.Join(", ", nodes)}");
}

Console.ReadKey();


void PrintGraph(Graph graph)
{
    foreach (Node node in graph.Nodes)
    {
        Console.Write(node.Value + " -> ");
        foreach (Node neighbor in node.Neighbors)
        {
            Console.Write(neighbor.Value + " ");
        }
        Console.WriteLine();
    }
}


int[] KosarajuAlg(Graph graph)
{
    var nodes = graph.Nodes;
    var visited = new bool[nodes.Count];
    var stack = new Stack<Node>();
    var result = new int[nodes.Count];

    // Посетить все узлы в графе
    foreach (var node in nodes)
    {
        if (!visited[node.Value])
        {
            DFS(node, visited, stack);
        }
    }

    Array.Fill(visited, false);

    // Назначить компоненты сильно связных узлов
    var component = 0;
    while (stack.Count > 0)
    {
        var node = stack.Pop();
        if (!visited[node.Value])
        {
            AssignComponent(node, visited, result, component);
            component++;
        }
    }

    return result;
}


void DFS(Node node, bool[] visited, Stack<Node> stack)
{
    if (visited[node.Value] || stack.Contains(node))
    {
        return;
    }

    visited[node.Value] = true;
    stack.Push(node);

    foreach (var neighbor in node.Neighbors)
    {
        DFS(neighbor, visited, stack);
    }
}


void AssignComponent(Node node, bool[] visited, int[] result, int component)
{
    visited[node.Value] = true;
    result[node.Value] = component;

    foreach (var neighbor in node.Neighbors)
    {
        if (!visited[neighbor.Value])
        {
            AssignComponent(neighbor, visited, result, component);
        }
    }
}


class Node
{
    public int Value { get; set; }
    public List<Node> Neighbors { get; set; }
    public bool Visited { get; set; }
}

class Graph
{
    public List<Node> Nodes { get; set; }

    public void Visualize()
    {
        foreach (var node in Nodes)
        {
            Console.WriteLine(node.Value + " -> " + string.Join(", ", node.Neighbors.Select(edge => edge.Value)));
        }
    }
}

class Stack<T>
{
    private T[] _array;
    private int _size;

    public int Count { get { return _size; } }

    public Stack()
    {
        _array = Array.Empty<T>();
    }

    public void Push(T element)
    {
        int size = _size;
        var array = _array;

        if (size < _array.Length)
        {
            array[size] = element;
            _size = size + 1;
        }
        else
        {
            Array.Resize(ref _array, size + 1);
            _array[_size] = element;
            _size++;
        }
    }

    public T Pop()
    {
        int size = _size - 1;

        if (size >= _array.Length)
        {
            throw new Exception("Стек пуст");
        }
       
        var item = _array[size];

        _array[size] = default!;
        _size = size;

        return item;
    }

    public bool Contains(T item)
    {
        return _size != 0 && Array.LastIndexOf(_array, item, _size - 1) != -1;
    }
}